// document.getElementById("fetch-button").addEventListener("click", async () => {
//   const outputDiv = document.getElementById("output");
//   outputDiv.textContent = "Loading...";
//
//   try {
//     // Collect selected ingredients
//     const selectedIngredients = [];
//     const ingredientButtons = document.querySelectorAll(
//       ".ingredient-button.clicked",
//     );
//     ingredientButtons.forEach((button) => {
//       selectedIngredients.push(button.getAttribute("data-ingredient"));
//     });
//
//     // Check if there are selected ingredients
//     if (selectedIngredients.length === 0) {
//       outputDiv.textContent = "Please select at least one ingredient.";
//       return;
//     }
//
//     // Send request to the server with selected ingredients
//     const response = await fetch("/api/openai", {
//       method: "POST",
//       headers: { "Content-Type": "application/json" },
//       body: JSON.stringify({ ingredients: selectedIngredients }),
//     });
//
//     const data = await response.json();
//
//     if (response.ok) {
//       const recipe = JSON.parse(data.message);
//
//       document.getElementById("recipe-name").textContent = recipe.name;
//
//       const ingredientsList = document.getElementById("recipe-ingredients");
//       ingredientsList.innerHTML = "";
//       recipe.ingredients.forEach((ingredient) => {
//         const li = document.createElement("li");
//         li.textContent = ingredient;
//         ingredientsList.appendChild(li);
//       });
//
//       const instructionsList = document.getElementById("recipe-instructions");
//       instructionsList.innerHTML = "";
//       recipe.instruction.split(". ").forEach((step) => {
//         const li = document.createElement("li");
//         li.textContent = step;
//         instructionsList.appendChild(li);
//       });
//
//       outputDiv.textContent = "";
//     } else {
//       outputDiv.textContent = "Error: " + data.error;
//     }
//   } catch (error) {
//     outputDiv.textContent = "Fetch error: " + error.message;
//   }
// });

// Add event listeners for ingredient buttons
const ingredientButtons = document.querySelectorAll(".ingredient-button");
ingredientButtons.forEach((button) => {
  button.addEventListener("click", () => {
    button.classList.toggle("clicked");
  });
});
