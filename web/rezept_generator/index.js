import OpenAI from "openai";
import express from "express";
import path from "path";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();
const port = 3000;

const openai = new OpenAI();

app.use(express.static(path.join(__dirname, "public")));

app.use(express.json());

app.get("/api/openai", async (req, res) => {
  // console.log("Request body:", req.body); // Log request body
  // const { ingredients } = req.body;
  //
  // // Check if ingredients are provided and are in an array
  // if (!ingredients || !Array.isArray(ingredients)) {
  //   return res.status(400).json({ error: "Invalid ingredients format" });
  // }
  //
  // const ingredientsList = ingredients.join(", ");

  // let inputContent = f`Gib mir nur einen Rezept in JSON-Format, die soll aus 3 Felder mit den Namen: 'name', 'ingridients' und 'instruction' bestehen , mit den folgenden Zutaten: ${ingredientsList}`;
  let inputContent = `Gib mir nur einen Rezept mit den folgenden Zutaten: salz, tomaten, mehl, rindfleisch`;

  try {
    const completion = await openai.chat.completions.create({
      messages: [
        {
          role: "system",
          content:
            "Du bist ein Assisten der ein Rezept mit den vorgegebenen Zutaten in JSON-Format ausgibt, die soll aus 3 Feldern mit den Name. 'name', 'ingredients' und 'instruction' bestehen",
        },
        {
          role: "user",
          content: inputContent,
        },
      ],
      model: "gpt-3.5-turbo",
      response_format: { type: "json_object" },
    });

    const responseContent = completion.choices[0].message.content;

    res.json({ message: responseContent });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

app.listen(port, () => {
  console.log(`Server läuft unter http://localhost:${port}`);
});
