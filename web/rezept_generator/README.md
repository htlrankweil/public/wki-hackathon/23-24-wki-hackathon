
# Rezeptfinder-Projekt

## Autoren
Dieses Projekt wurde von Ercüment Cetin und Eren Güngörmüs entwickelt.


## Projektbeschreibung

Dieses Projekt ermöglicht es, mithilfe der OPENAI API Rezeptvorschläge basierend auf ausgewählten Zutaten zu erhalten. Derzeit müssen die Zutaten manuell im Code angegeben werden. Ein OPENAI API Key wird benötigt und muss als Systemumgebungsvariable gesetzt werden.

## Inbetriebnahme

1. Alle benötigten Node-Module herunterladen:
    ```bash
    npm install
    ```

2. Das Projekt im aktuellen Arbeitsverzeichnis starten:
    ```bash
    node .
    ```

## Bekannte Fehler

- Das Auswählen von Zutaten auf der Webseite fügt diese derzeit nicht in den Prompt ein.

## Mögliche Erweiterungen

- Eine Funktion, mit der man neue Zutaten hinzufügen kann, die dann automatisch als Buttons generiert werden.
- Eine Suchleiste für eine benutzerfreundliche Steuerung.
