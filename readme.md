# KI Hackathon @ Alphagate 2024

Dieses Repository beinhaltet die Projekte, die während des KI Hackathons vom 21.06. bis 22.06. bei der Firma Alphagate entstanden sind.

## Projekte

|                          Projekt (Doku Link)                           | Technologie |                  Pfad                   |
| :--------------------------------------------------------------------- | :---------: | :-------------------------------------- |
| [Face Color Changer](web/facecolor_changer/README.md)                  |     Web     | `web/facecolor_changer`                 |
| [Rezept Generator](web/rezept_generator/README.md)                     |     Web     | `web/rezept_generator`                  |
| [Schiffe versenken](web/battleship/README.md)                          |     web     | `web/battleship`                        |
| [Gestensteuerung](python/gestensteuerung/readme.md)                    |   Python    | `python/gestensteuerung`                |
| [Pong Ping](python/pongping/README.md)                                 |   Python    | `python/pongping`                       |
| [Rock Paper Scissors](python/rock_paper_scissors_against_ai/README.md) |   Python    | `python/rock_paper_scissors_against_ai` |

## Voraussetzungen

Jedes Projekt hat eigene Voraussetzungen die benötigt werden.

### Python

Für die Python Projekte können die jeweiligen `requirements.txt` Dateien für die Voraussetzungen verwendet werden. Alternativ kann auch die `requirements.txt` aus dem Hauptverzeichnis verwendet werden, hier sind die Voraussetzungen aller Projekte gebündelt. Für die Installation der Requirements aus einer Textdatei kann folgender Befehl verwendet werden:

```bash
pip install -r requirements.txt
```

Nach der Installation können die einzelne Python Projekte direkt aus der Command Line gestartet werden.

**Wichtig:** Es muss immer in den entsprechenden Projektordner gewechselt werden, damit die benötigten Dateien gefunden werden.

```bash
python <name-der-datei>.py
```

**Pong Ping:**

```bash
cd python/pongping
python PongPing.py
```

**Gestensteuerung:**

```bash
cd python/gestensteuerung
python gesturedetection.py
```

**Rock Paper Scissors:**

```bash
cd python/rock_paper_scissors_against_ai
python RPS.py
```

### Web

Das Projekt *Face Color Changer* benötigt keine weiteren Voraussetzungen.

Das Projekt *Rezept Generator* benötigt zusätzliche npm Module. Dazu folgende Befehle aus dem Projektordner ausführen:

1. Alle benötigten Node-Module herunterladen:

    ```bash
    npm install
    ```

2. Das Projekt im aktuellen Arbeitsverzeichnis starten:

    ```bash
    node .
    ```
