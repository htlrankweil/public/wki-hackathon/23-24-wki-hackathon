# Wie spielt man?
Zum Starten einfach das Programm ausführen.
Um die Hand zu erkennen, muss sie zuerst einmal vollständig im Bild sein.
Es sollten gute Lichtverhältnisse bestehen. Da das Programm sonst schwierigkeiten hat, die Hand zu erkennen.
Sobald eine Hand erkannt wird, wird zwischen dem Daumen und dem Zeigefinger eine rote Linie gezogen, welche als Schläger funktionieren.

# Anpassen des Spiels
Es können, bei Bedarf verschiedene Parameter modifiziert werden. 

# Ball-Parameter
ball_radius = 20  # Radius des Balls
ball_color = (0, 0, 255)  # Farbe des Balls (Rot)
ball_position = None  # Startposition des Balls (noch nicht gesetzt)
initial_speed = 5  # Anfangsgeschwindigkeit des Balls
ball_velocity = [0, 0]  # Anfangsrichtung des Balls
velocity_increase_factor = 1.1  # Geschwindigkeitszunahme bei Kollision

# Bildschirm auflösung anpassen
screen_width = 1280  # Setze Bildschirmbreite
screen_height = 720  # Setze Bildschirmhöhe

# Werte hier ändern, um die Farbe der Punktezahl zu ändern
# Der erste Wert ist die Blau stärke, der Zweite die Grün stärke und der Letzte die Rot stärke
score_color = (255, 0, 0)

# Hotkeys können ebenfalls bei Bedarf geändert werden 
  key = cv2.waitKey(1) & 0xFF
  # Beenden das Spiel
  if key == ord('q'): 
    break
# Setzte den Ball zurück in die Mitte des Spielfeldes
  elif key == ord('s'): 
    reset_ball()
# Setzt das Spiel zurück (alles ausßgenommen Highscore)
  elif key == ord('r'): 
    reset_game()