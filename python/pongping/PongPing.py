import cv2
import mediapipe as mp
import random
import math
from webcamdetect import list_cameras

# Kamera wählen
cameras = list_cameras()
print("Verfügbare Kameras:")
for i, camera in enumerate(cameras):
    print(f"[{i}] Kamera {camera}")

camera_index = 0
if len(cameras) > 1:
    camera_index = int(input("Bitte wählen Sie eine Kamera: "))

# Initialisiere MediaPipe Hand-Module
mp_hands = mp.solutions.hands  # Lade das MediaPipe Hands Modul
hands = mp_hands.Hands(max_num_hands=4, min_detection_confidence=0.7, min_tracking_confidence=0.7)  # Konfiguriere Handerkennung
mp_draw = mp.solutions.drawing_utils  # Werkzeug zum Zeichnen der Handverbindungen

# Starte Video-Capture von der Webcam (Index 0)
cap = cv2.VideoCapture(camera_index)  # Öffne die Kamera

# Ball-Parameter
ball_radius = 20  # Radius des Balls
ball_color = (0, 0, 255)  # Farbe des Balls (Rot)
ball_position = None  # Startposition des Balls (noch nicht gesetzt)
initial_speed = 5  # Anfangsgeschwindigkeit des Balls
ball_velocity = [0, 0]  # Anfangsrichtung des Balls
velocity_increase_factor = 1.1  # Geschwindigkeitszunahme bei Kollision

# Spielrahmen Parameter
FRAME_MARGIN = 10  # Sicherheitsabstand vom Rand des Rahmens

# Spieler Punkte
player1_score = 0  # Punktestand Spieler 1
player2_score = 0  # Punktestand Spieler 2
highscore = 0  # Höchster Punktestand

# Funktion zur Berechnung der Reflexion des Balls
def reflect_ball(velocity, normal):
    normal_length = math.sqrt(normal[0]**2 + normal[1]**2)  # Berechne die Länge des Normalvektors
    if normal_length == 0:
        return velocity  # Keine Reflexion möglich, Rückgabe der ursprünglichen Geschwindigkeit
    normal = [n / normal_length for n in normal]  # Normiere den Normalvektor
    dot_product = velocity[0] * normal[0] + velocity[1] * normal[1]  # Berechne das Punktprodukt
    return [
        velocity[0] - 2 * dot_product * normal[0],  # Berechne die Reflexion in x-Richtung
        velocity[1] - 2 * dot_product * normal[1]   # Berechne die Reflexion in y-Richtung
    ]

# Funktion zum Zurücksetzen des Balls
def reset_ball():
    global ball_position, ball_velocity
    ball_position = [screen_width // 2, screen_height // 2]  # Setze den Ball in die Mitte des Bildschirms
    ball_velocity = [random.choice([initial_speed, -initial_speed]), random.choice([initial_speed, -initial_speed])]  # Zufällige Startgeschwindigkeit

# Funktion zum Zurücksetzen des Spiels
def reset_game():
    global player1_score, player2_score, highscore
    player1_score = 0  # Setze Punkte für Spieler 1 zurück
    player2_score = 0  # Setze Punkte für Spieler 2 zurück
    reset_ball()  # Setze den Ball zurück

# Berechnung der Entfernung zwischen zwei Punkten
def distance(point1, point2):
    return math.sqrt((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2)

# Lese einen Frame vom Video, um die Höhe und Breite zu bestimmen
success, img = cap.read()
if success:
    img = cv2.flip(img, 1)  # Spiegel das Bild
    h, w, c = img.shape  # Erhalte Bildabmessungen
    screen_width = 1280  # Setze Bildschirmbreite
    screen_height = 720  # Setze Bildschirmhöhe
    reset_ball()  # Setze den Ball zurück

# Hauptschleife für das Spiel
while True:
    success, img = cap.read()
    if not success:
        break

    img = cv2.flip(img, 1)  # Spiegel das Bild
    img = cv2.resize(img, (screen_width, screen_height))  # Passe die Bildgröße an
    img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Konvertiere BGR zu RGB
    results = hands.process(img_rgb)  # Verarbeite das Bild zur Handerkennung

    # Zeichne den Spielrahmen
    cv2.rectangle(img, (FRAME_MARGIN, FRAME_MARGIN), (screen_width - FRAME_MARGIN, screen_height - FRAME_MARGIN), (255, 255, 255), 5)

    # Wenn Hände erkannt wurden
    if results.multi_hand_landmarks:
        for hand_landmarks in results.multi_hand_landmarks:
            # Extrahiere Landmark-Positionen für Zeigefingerspitze und Daumenspitze
            index_finger_tip_pos = (int(hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].x * screen_width),
                                    int(hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].y * screen_height))
            thumb_tip_pos = (int(hand_landmarks.landmark[mp_hands.HandLandmark.THUMB_TIP].x * screen_width),
                             int(hand_landmarks.landmark[mp_hands.HandLandmark.THUMB_TIP].y * screen_height))

            # Zeichne eine rote Linie zwischen Zeigefingerspitze und Daumenspitze
            cv2.line(img, index_finger_tip_pos, thumb_tip_pos, (0, 0, 255), 2)

            # Berechne den Normalvektor der Linie
            line_vector = (thumb_tip_pos[0] - index_finger_tip_pos[0], thumb_tip_pos[1] - index_finger_tip_pos[1])
            normal_vector = (-line_vector[1], line_vector[0])

            safety_distance = 0.1  # Sicherheitsabstand zwischen Ball und Linie

            if ball_position:
                x1, y1 = index_finger_tip_pos
                x2, y2 = thumb_tip_pos
                ball_x, ball_y = ball_position

                # Berechne den Abstand des Balls zur Linie
                denominator = (y2 - y1)**2 + (x2 - x1)**2 + safety_distance
                distance_to_line = abs((y2 - y1) * ball_x - (x2 - x1) * ball_y + x2 * y1 - y2 * x1) / math.sqrt(denominator)

                # Überprüfe Kollision mit der Linie
                if distance_to_line <= ball_radius:
                    line_length = distance(index_finger_tip_pos, thumb_tip_pos)
                    if distance(index_finger_tip_pos, (ball_x, ball_y)) <= line_length and distance(thumb_tip_pos, (ball_x, ball_y)) <= line_length:
                        ball_velocity = reflect_ball(ball_velocity, normal_vector)  # Reflexion des Balls
                        ball_velocity[0] *= velocity_increase_factor  # Erhöhe die Geschwindigkeit
                        ball_velocity[1] *= velocity_increase_factor
                
                # Überprüfe Kollision mit den Endpunkten
                if distance((ball_x, ball_y), index_finger_tip_pos) <= ball_radius or distance((ball_x, ball_y), thumb_tip_pos) <= ball_radius:
                    normal_vector_endpoint = [ball_x - x1, ball_y - y1] if distance((ball_x, ball_y), index_finger_tip_pos) <= ball_radius else [ball_x - x2, ball_y - y2]
                    ball_velocity = reflect_ball(ball_velocity, normal_vector_endpoint)
                    ball_velocity[0] *= velocity_increase_factor
                    ball_velocity[1] *= velocity_increase_factor

    # Bewege den Ball basierend auf seiner Geschwindigkeit
    if ball_position:
        ball_position[0] += int(ball_velocity[0])
        ball_position[1] += int(ball_velocity[1])

        # Kollision mit dem linken oder rechten Rand
        if ball_position[0] - ball_radius <= FRAME_MARGIN:
            player2_score += 1  # Punkt für Spieler 2
            reset_ball()  # Setze den Ball zurück
        elif ball_position[0] + ball_radius >= screen_width - FRAME_MARGIN:
            player1_score += 1  # Punkt für Spieler 1
            reset_ball()  # Setze den Ball zurück

        # Kollision mit dem oberen oder unteren Rand
        if ball_position[1] - ball_radius <= FRAME_MARGIN:
            ball_position[1] = FRAME_MARGIN + ball_radius
            ball_velocity[1] = -ball_velocity[1]  # Reflexion
        elif ball_position[1] + ball_radius >= screen_height - FRAME_MARGIN:
            ball_position[1] = screen_height - FRAME_MARGIN - ball_radius
            ball_velocity[1] = -ball_velocity[1]  # Reflexion

        # Zeichne den Ball
        cv2.circle(img, tuple(ball_position), ball_radius, ball_color, -1)

    # Aktualisiere den Highscore während des Spiels
    highscore = max(highscore, player1_score, player2_score)

    # Zeige die Punkte der Spieler
    score_color = (255, 0, 0)  # Blau
    cv2.putText(img, f"Player 1: {player1_score}", (50, 50), cv2.FONT_HERSHEY_COMPLEX, 1, score_color, 2)
    cv2.putText(img, f"Player 2: {player2_score}", (screen_width - 300, 50), cv2.FONT_HERSHEY_COMPLEX, 1, score_color, 2)
    
    # Zeige den Highscore
    cv2.putText(img, f"Highscore: {highscore}", (screen_width // 2 - 150, 50), cv2.FONT_HERSHEY_COMPLEX, 1, score_color, 2)

    # Überprüfe die Tasteneingaben
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):  # Beenden des Spiels
        break
    elif key == ord('s'):  # Starten des Spiels
        reset_ball()
    elif key == ord('r'):  # Zurücksetzen des Spiels
        reset_game()

    # Zeige das Bild mit den Zeichnungen
    cv2.imshow("Handerkennung", img)

# Release die Kamera und schließe alle Fenster
cap.release()
cv2.destroyAllWindows()
