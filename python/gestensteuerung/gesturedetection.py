import cv2
import mediapipe as mp
from input import Keyinput, Mouseinput
from Filemanager import Filemanager
import os
from keras.models import load_model
import numpy as np 
from screeninfo import get_monitors
import pygetwindow as gw
import pyttsx3
import ctypes
import time

def clear_capture(capture):
    capture.release()
    cv2.destroyAllWindows()

def count_cameras():
    n = 0
    for i in range(10):
        try:
            cap = cv2.VideoCapture(i)
            ret, frame = cap.read()
            cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            clear_capture(cap)
            n += 1
        except:
            clear_capture(cap)
            break
    return n

def predict(landies):
    landi_arr = []
    for landi in landies:
        landi_arr.append(landi.x)
        landi_arr.append(landi.y)
    lanid_arr_np = np.array(landi_arr).reshape(1, -1)
    #print(lanid_arr_np)
    
    return model.predict(lanid_arr_np)

def onehotdecoder(oh_data):
    
    
    index = oh_data[0].argmax()
    #print(f'Possibilities: {possibilities}')
    return possibilities[index]
    
def next_camera(cap, camera_idx):
    if camera_count <= 1:
        return

    if cap.isOpened():
        cap.release()

    camera_idx = (camera_idx + 1) % camera_count    
    
    cap = cv2.VideoCapture(camera_idx)
    
    while not cap.isOpened():
        time.sleep(0.5)
    
    return cap, camera_idx


# Constants
window_name = "Gestensteuerung"
camera_idx = 0

#Modell laden
filepath = 'model.keras'
model = load_model(filepath) 

# Onehotencoder lables laden
fm = Filemanager()
possibilities = fm.load_class_names('class_names.csv') # 

# Initialize MediaPipe hands module
mp_hands = mp.solutions.hands
mp_drawing = mp.solutions.drawing_utils

# tts init
engine = pyttsx3.init()

for m in get_monitors(): # get display properties
    display_width = m.width
    display_heigth= m.height
    #print(m.width, m.height)

kinput = Keyinput()
minput = Mouseinput(Displaywidth=display_width, Displayheight=display_heigth)
# Initialize webcam
camera_count = count_cameras()

print(f"Camera count: {camera_count}")

cap = cv2.VideoCapture(0)

with mp_hands.Hands(
    static_image_mode=False,
    max_num_hands=1, #david brucht 2, gestenseuerung nid
    min_detection_confidence=0.5,
    min_tracking_confidence=0.5) as hands:

    capture = False

    last_hand_pos_x = 0
    last_hand_pos_y = 0

    prevgesture = ""
    prevgesture_count = 0
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break


        text = "nix"

        
        
        # Convert the BGR image to RGB
        rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        # Process the frame and find hands
        results = hands.process(rgb_frame)

        # Draw hand landmarks and connections
        if results.multi_hand_landmarks:
            #print(f'Hand count: {len(results.multi_hand_landmarks)}')
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)

                landmarks = hand_landmarks.landmark # aus landmarks gesten erkennen
                oh_res = predict(landmarks)
                text_res = onehotdecoder(oh_res)

                # get hand position
                hand_pos_x = landmarks[5].x
                hand_pos_y = landmarks[5].y
                text = text_res

                minanzahl_fuer_erkennung = 5

                # gestenaktionen wo durchgehend usgführt wörrend
                if(text_res == 'finger_up'):
                            #hand_pos_delta_x = np.abs(landmarks[5].x - last_hand_pos_x)
                            #hand_pos_delta_y =  np.abs(landmarks[5].y - last_hand_pos_y)
                            #last_hand_pos_x = landmarks[5].x
                            #last_hand_pos_y = landmarks[5].y
                            #if(hand_pos_delta_x < 20 and landmarks[5].x < 20):
                            minput.setmousepositionrelative(hand_pos_x, hand_pos_y)
                if(prevgesture_count > minanzahl_fuer_erkennung +1): # beim schweigefuchs die lautstärke steuern
                    if(text_res == 'schweigefuchs'):
                        kinput.volumedown()

                # anzahlerkennung
                if(text_res == prevgesture):
                    prevgesture_count += 1
                else:
                    prevgesture_count = 0
                prevgesture = text_res

                # gesten wo nur oamal usgführt wörra söttend
                if(prevgesture_count == minanzahl_fuer_erkennung):
                    if(text_res == 'mittelfinger'):
                        print('mittelfinger')
                        exit('programm über geste beendet')
                    if(text_res == 'thumbs_side' or text_res == 'fist'):
                        minput.leftclick()
                    if(text_res == 'thumbs_up'):
                        minput.rightclick()
                    if(text_res == 'peace'):
                        minput.doubleclick()
                    #if(text_res == 'schweigefuchs'):
                        #engine.say("Bitte ein wenig ruhiger")
                        #engine.runAndWait()


        fliped_frame = cv2.flip(frame,1) #Bild spiegeln

        font = cv2.FONT_HERSHEY_SIMPLEX # font 
        org = (50, 50) # org 
        fontScale = 0.5# fontScale 
        color = (0, 0, 255) # Red color in BGR 
        thickness = 2 # Line thickness of 2 px 
  
        frame_with_text = cv2.putText(fliped_frame, text, org, font,  
                   fontScale, color, thickness, cv2.LINE_AA) 

        # Display the resulting frame
        cv2.imshow(window_name, frame_with_text)
        
        #cv2.resizeWindow(window_name, 400, 300)


        # Steuerung
        key =  cv2.waitKey(1) & 0xFF
        if key == ord('q'):  # Exit when 'q' is pressed
            break
        elif key == ord('c'): # change camera
            cap, camera_idx = next_camera(cap, camera_idx)

# Release the webcam and close windows
cap.release()
cv2.destroyAllWindows()