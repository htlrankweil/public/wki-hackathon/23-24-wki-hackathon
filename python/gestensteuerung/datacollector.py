import cv2
import mediapipe as mp
from Filemanager import Filemanager
import os

# Initialize MediaPipe hands module
mp_hands = mp.solutions.hands
mp_drawing = mp.solutions.drawing_utils

# Initialize webcam
cap = cv2.VideoCapture(0)



def ui():
    print('Welcome to the Gesture Data collector')
    print('----')

    # Alle CSV files in dem Ordner auslesen und speichern
    dirlist = os.listdir()
    filtered_dir_list = []
    for filename in dirlist:
        if filename.endswith('.csv'):
            filtered_dir_list.append(filename)



    print(f'available Files: ')
    for i in range(len(filtered_dir_list)):
        print(f'[{i}]: {filtered_dir_list[i]}')
    print('Input a FileName or select an existing one using a number:')
    inp = input()
    if(inp.isnumeric()):
        filename = filtered_dir_list[int(inp)]
        
    else:
        filename = inp
    print(f'selected: {filename}')

    print('write a label you want to train:')
    label = input()
    return filename, label

# die funktion isch eig richtig sinnfrei
#def savelandmarks(label, hand_landmarks):
#    landmarks = hand_landmarks.landmark
#    fm.save_to_csv(landmarks, label)

filename, label = ui() #consolen dialog für filename und label
fm = Filemanager(filename)

with mp_hands.Hands(
    static_image_mode=False,
    max_num_hands=1, #david brucht 2, gestenseuerung nid
    min_detection_confidence=0.5,
    min_tracking_confidence=0.5) as hands:

    capture = False

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        # Convert the BGR image to RGB
        rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        # Process the frame and find hands
        results = hands.process(rgb_frame)

        # Draw hand landmarks and connections
        if results.multi_hand_landmarks:
            #print(f'Hand count: {len(results.multi_hand_landmarks)}')
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)
                # save data here
                if(capture):
                    #savelandmarks(label, hand_landmarks)
                    landmarks = hand_landmarks.landmark
                    fm.save_to_csv(landmarks, label)

        fliped_frame = cv2.flip(frame,1) #Bild spiegeln

        font = cv2.FONT_HERSHEY_SIMPLEX # font 
        org = (50, 50) # org 
        fontScale = 1# fontScale 
        color = (0, 0, 255) # Red color in BGR 
        thickness = 2 # Line thickness of 2 px 

        if(capture):
            text = f"capturing: {label}"
        else:
            text = f"not recording ({label})"

        frame_with_text = cv2.putText(fliped_frame, text, org, font,  
                   fontScale, color, thickness, cv2.LINE_AA) 

        # Display the resulting frame
        cv2.imshow('Gesture Recognition', frame_with_text)

        # Steuerung
        key =  cv2.waitKey(1) & 0xFF
        if  key == ord('c'):
            capture = True
            print('Start capturing')
        if key == ord('s'):
            capture = False
            print('stopped capturing')
        if key == ord('q'):  # Exit when 'q' is pressed
            break
        if key == ord('n'):
            print('input new label:')
            label = input()

# Release the webcam and close windows
cap.release()
cv2.destroyAllWindows()