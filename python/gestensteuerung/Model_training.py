# Drive einbinden
# from google.colab import drive
# drive.mount('/content/drive/')

# Libraries einbinden
import numpy as np
import tensorflow as tf
from keras.models import Sequential, load_model # type: ignore
from keras.layers import Input, Dense # type: ignore
#from keras._tf_keras.keras.models import load_model


import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split
from Filemanager import Filemanager


# Pfad der Daten definieren
filename_data = r'gestures.csv'
filepath_save_model = r'model.keras'
filepath_save_classes = r'class_names.csv'

data = pd.read_csv(filename_data)
features = data.drop(['label'], axis=1)



data.sample(3)
#data.columns
data.head()
data.info()
data.describe()

#sns.set()
#sns.pairplot(data, hue="label")



# one hot encoding der Labels => Jede Klasse bekommt eine eigene Spalte
# Jede Datenzeile wird One Hot encoded
# => Mapping des Klassennamen zu einer 1 in der richtigen Spalte
# One Hot Encoding wird unsere Ground Truth (Wahrheit/Zielwert beim Training)
encoder = OneHotEncoder()
labels_oh = encoder.fit_transform(data[['label']]).toarray()

# Gelernte Klassen des OneHotEncoders holen
class_names = encoder.categories_[0]
print(class_names)

# Ersten 5 Zeilen ausgeben
print(labels_oh[:5])

# Daten aufteilen


# 60% ... training, 40% ... validation und test
feat_trn, feat_val_tst, labels_trn, labels_val_tst = train_test_split(features, labels_oh, test_size=0.4)
# Validation und Test aufteilen
feat_val, feat_tst, labels_val, labels_tst = train_test_split(feat_val_tst, labels_val_tst, test_size=0.5)

print("Training size: ", feat_trn.shape[0])
print("Validation size: ", feat_val.shape[0])
print("Test size: ", feat_tst.shape[0])


num_inputs = feat_trn.shape[1]  # Anzahl Spalten der Features
num_outputs = labels_trn.shape[1] # Anzahl Spalten des One Hot Encodings

print(f"Inputs: {num_inputs}, Outputs: {num_outputs}")




# Mini Neuronales Netz zusammenbauen
model = Sequential()  # Erstellt ein neues Sequential Model Objekt
# Input Layer (Schicht) hinzufügen
model.add(Input((num_inputs,)))  # 4 Input Nodes (Knoten) erstellen
# Hidden Layer hinzufügen
model.add(Dense(10, activation='relu')) # 5 hidden nodes
model.add(Dense(10, activation='relu')) # 5 hidden nodes
# Output Layer hinzufügen
model.add(Dense(num_outputs, activation='softmax'))

# Softmax
# Ausgänge vor Softmax: [1, 2, 1] ... 4
# Ausgänge nach Softmax: [1, 2, 1] / 4 = [0.25, 0.5, 0.25]
# Summe nach Softmax = 1

# Model compilieren => fürs Training und die Prediction vorbereiten
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Hyperparameter fürs Trining festlegen
epochs = 100       # Anzahl an Trainingsepochen,
# während einer Epoche sieht das Netz alle Trainingsdaten 1x
batch_size = 16  # Anzahl an Trainingsdaten,
# die das Netz sieht, bevor die Gewichte angepasst werden

# training starten
history = model.fit(feat_trn, labels_trn, epochs=epochs, batch_size=batch_size, validation_data=(feat_val, labels_val))


# Modell speichern
def save_model(model, file_path):
   
    model.save(file_path)
    print(f"Modell wurde unter '{file_path}' gespeichert.")

save_model(model, filepath_save_model)


# class_names speichern
filemanger = Filemanager()
filemanger.save_class_names(class_names, filepath_save_classes)


tst_loss, tst_acc = model.evaluate(feat_tst, labels_tst)

print("Test Loss:", tst_loss)
print("Test Accuracy:", tst_acc)

# Anzahl falsch klassifiziert
num_test_data = feat_tst.shape[0]
num_wrong = num_test_data - num_test_data * tst_acc
print("Anzahl falsch:", int(num_wrong))



# Visualize the evaluation curves
# Extract training history
training_loss = history.history['loss']
validation_loss = history.history['val_loss']
training_accuracy = history.history['accuracy']
validation_accuracy = history.history['val_accuracy']
epochs = range(1, len(training_loss) + 1)

# # Plot loss curves
# plt.figure(figsize=(12, 5))
# plt.subplot(1, 2, 1)
# sns.lineplot(training_loss, label='Training Loss')
# sns.lineplot(validation_loss, label='Validation Loss')
# plt.title('Loss')
# plt.xlabel('Epochs')
# plt.ylabel('Loss')
# plt.legend()

# # Plot accuracy curves
# plt.subplot(1, 2, 2)
# sns.lineplot(training_accuracy, label='Training Accuracy')
# sns.lineplot(validation_accuracy, label='Validation Accuracy')
# plt.title('Accuracy')
# plt.xlabel('Epochs')
# plt.ylabel('Accuracy')
# plt.legend()

# plt.tight_layout()
# plt.show()

##################################


# Testdaaten predicten
pred_tst = model.predict(feat_tst)
# Index mit dem Maximum
pred_tst[0].argmax()
# Über eine genze Spalte das Max finden
pred_label_index = pred_tst.argmax(axis=1)   # axis ... über welche Achse der matrix (0... zeilen, 1... spalten), soll das argmax berechnet werden
# Mapping der Klassennamen aufgrund des maximalen Ausgangs (Index) unseres Netzes
pred_label = class_names[pred_label_index]
print(pred_label)

# Vergleich
label_tst_classes = class_names[labels_tst.argmax(axis=1)]




