import pandas as pd
import numpy as np
import os
import csv

class Filemanager():
    def __init__(self, filename='data.csv'):
        self.filename = filename
        self.headerstring = 'x0,y0,x1,y1,x2,y2,x3,y3,x4,y4,x5,y5,x6,y6,x7,y7,x8,y8,x9,y9,x10,y10,x11,y11,x12,y12,x13,y13,x14,y14,x15,y15,x16,y16,x17,y17,x18,y18,x19,y19,x20,y20,label'

    def save_to_csv(self,dataarray, label):
        
        line = ""
        #Daten vorbereiten
        for i in range(len(dataarray)):
            #print(f'länge {len(dataarray)}')
            line += str(dataarray[i].x) + ',' + str(dataarray[i].y) + ','
        line += label
        

        # Überprüfe, ob die Datei bereits existiert
        if os.path.exists(self.filename):
            # Wenn die Datei existiert, füge die Daten an
            with open(self.filename, 'a') as file:
                file.write('\n' + line)
        else:
            # Wenn die Datei nicht existiert, schreibe die Daten und füge einen Header hinzu
            with open(self.filename, 'w') as file:
                file.write(self.headerstring + '\n' + line)
    
    def save_class_names(self, class_names, filepath):
        with open(filepath, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(class_names)

    def load_class_names(self, filepath):
        arr = np.loadtxt(filepath,delimiter=",", dtype=str)
        return arr
    
