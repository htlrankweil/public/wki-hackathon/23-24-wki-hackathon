# Gestensteuerung KI Hackathron 21 - 21.6.2024

Team: David Hager, Florian lerchemüller, Fabio Scheffknech

Github: https://github.com/lerchenflo/gestensteuerung

# Gestensteuerung

gesturedetection.py:
1. darauf achten dass die originalen dateien `model.keras` und `class_names.csv` vorhanden sind
1. gesturedetection.py ausführen
1. Tastenbelegung:
   1. 'c' nächste Kamera
   2. 'q' beenden

|            Gesete            |          aktion          |
| ---------------------------- | ------------------------ |
| Finger nach oben zeigen      | Mausposition steuern     |
| Faust / Daumen auf der seite | Mausklick                |
| Peace zeichen                | Doppelklick              |
| Daumen nach oben             | Rechtsklick              |
| Schweigefuchs                | Lautstärke auf 0 stellen |
| Mittelfinger                 | Programm beenden         |

# Testdaten generieren

datacollector.py:

1. datacollector.py öffnen
1. name für die datei eingeben, bzw. über index vorhandene datei auswählen
1. zu trainierendes label eingeben
1. mit 'c' (für capture) beginnen daten aufzunehme
1. mit 's' (für stopp) die aufnahme pausieren/stoppen
1. mit 'n' (für neues label) in der Konsole neues Label eingeben
1. mit c und s weiter training starten bzw. stoppen
1. mit 'q' (für quit) beenden

| tasteneingabe |             aktion              |
| ------------- | ------------------------------- |
| c (caputre)   | aufnahmen starten               |
| s (stop)      | datenaunahme stoppen/ pausieren |
| n (new label) | neues label in konsole eingeben |
| q (quit)      | programm beenden                |

# Modell Trainieren

Model_training.py:

1. in Model_training.py in zeile 20 - 22 dateinahmen evt. anpassen
1. (optional: trainingsparameter anpassen)
1. Datei ausführen

# trainierte gesten erkennen

gesturedetection.py : 
1. in zeile 17 und 22 evt. Dateinnahmen abändern
1. gesturedetection.py ausführen
1. mit 'q' taste beenden

Achtung! bei bestimmten labels könnten maus und tastaturaktionen auseführt werden!
