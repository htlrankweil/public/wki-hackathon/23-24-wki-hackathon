from pynput.keyboard import Key, Controller as Keyboardcontroller
from pynput.mouse import Button, Controller as MouseController
import subprocess
import time

class Keyinput():
    def __init__(self):
        self.keyboard = Keyboardcontroller()
        

    #Ausloggen aus Windows
    def logout(self):
        #Keys drücken
        self.keyboard.press(Key.cmd)
        self.keyboard.press('l')
        #Keys releasen
        time.sleep(1)
        self.keyboard.release('l')
        self.keyboard.release(Key.cmd)

    #Hinunterscrollen
    def down(self):
        self.keyboard.press(Key.down)
        time.sleep(0.1)
        self.keyboard.release(Key.down)

    #Hinaufscrollen
    def up(self):
        self.keyboard.press(Key.up)
        self.keyboard.release(Key.up)

    #Desktop anzeigen
    def showdesktop(self):
        #Keys drücken
        self.keyboard.press(Key.cmd)
        self.keyboard.press('d')
        #Keys releasen
        self.keyboard.release(Key.cmd)
        self.keyboard.release('d')

    #Windows Key drücken
    def windowskey(self):
        self.keyboard.press(Key.cmd)
        self.keyboard.release(Key.cmd)

    #Programm schließen
    def close(self):
        #Keys drücken
        self.keyboard.press(Key.alt)
        self.keyboard.press(Key.f4)
        #Keys releasen
        self.keyboard.release(Key.alt)
        self.keyboard.release(Key.f4)

    #Windows file Explorer starten
    def explorer(self):
        #Keys drücken
        self.keyboard.press(Key.cmd)
        self.keyboard.press('e')
        #Keys releasen
        self.keyboard.release(Key.cmd)
        self.keyboard.release('e')

    #Zwischenablage
    def clipboard(self):
        #Keys drücken
        self.keyboard.press(Key.cmd)
        self.keyboard.press('v')
        #Keys releasen
        self.keyboard.release(Key.cmd)
        self.keyboard.release('v')


    #Einen Desktop nach rechts
    def nextdesktop(self):
        #Keys drücken
        self.keyboard.press(Key.ctrl)
        self.keyboard.press(Key.cmd)
        self.keyboard.press(Key.right)
        #Keys releasen
        self.keyboard.release(Key.ctrl)
        self.keyboard.release(Key.cmd)
        self.keyboard.release(Key.right)

    #Einen Desktop nach links
    def previousdesktop(self):
        #Keys drücken
        self.keyboard.press(Key.ctrl)
        self.keyboard.press(Key.cmd)
        self.keyboard.press(Key.left)
        #Keys releasen
        self.keyboard.release(Key.ctrl)
        self.keyboard.release(Key.cmd)
        self.keyboard.release(Key.left)
    
    def volumedown(self):
        for i in range(10):
            self.keyboard.press(Key.media_volume_down)
            self.keyboard.release(Key.media_volume_down)
            #time.sleep(0.1)




class Mouseinput():
    def __init__(self, Displaywidth = 1920, Displayheight = 1080):
        self.mouse = MouseController()
        self.displaywidth = Displaywidth
        self.displayheight = Displayheight

    # x/y Koordinate anfahren
    def setmouse(self, X, Y):
        self.mouse.position = (X,Y)

    # x/y Koordinate anfahren, relativ zur Bildschirmgröße
    def setmousepositionrelative(self, x, y):   
        mouse_x = (((1-x)-0.1)*1.22)*self.displaywidth
        mouse_y = int(((y-0.2)*1.5) * self.displayheight)
        self.setmouse(mouse_x, mouse_y)
    
    #Linke Maustaste drücken
    def leftclick(self):
        # Press and release
        self.mouse.press(Button.left)
        self.mouse.release(Button.left)

    #Rechte Maustaste drücken
    def rightclick(self):
        # Press and release
        self.mouse.press(Button.right)
        self.mouse.release(Button.right)

    def doubleclick(self):
        self.leftclick()
        self.leftclick()

class CmdInput():
    def __init__(self):
        self.proc = subprocess.Popen('cmd.exe', stdin = subprocess.PIPE, stdout = subprocess.PIPE)
    # es tuat anscheinend nid aber i hab koan bock mehr
    def dir(self):
        self.proc.communicate('dir c:\\')
