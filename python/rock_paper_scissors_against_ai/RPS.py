import cv2
import mediapipe as mp
import numpy as np
import pickle
import tensorflow as tf
import random
import time
import os

from webcamdetect import list_cameras

# Kamera wählen
cameras = list_cameras()
print("Verfügbare Kameras:")
for i, camera in enumerate(cameras):
    print(f"[{i}] Kamera {camera}")

camera_index = 0
if len(cameras) > 1:
    camera_index = int(input("Bitte wählen Sie eine Kamera: "))


# Initialize hand detection
mp_hands = mp.solutions.hands
hands = mp_hands.Hands(min_detection_confidence=0.7, min_tracking_confidence=0.7)
mp_drawing = mp.solutions.drawing_utils

# Directories for data
data_dir = r'./'
if not os.path.exists(data_dir):
    os.makedirs(data_dir)

# Load images
rock_image_path = os.path.join(data_dir, '1.png')
paper_image_path = os.path.join(data_dir, '2.png')
scissors_image_path = os.path.join(data_dir, '3.png')

# Check if images are loaded correctly
rock_image = cv2.imread(rock_image_path)
if rock_image is None:
    print(f"Error loading rock image from {rock_image_path}")

paper_image = cv2.imread(paper_image_path)
if paper_image is None:
    print(f"Error loading paper image from {paper_image_path}")

scissors_image = cv2.imread(scissors_image_path)
if scissors_image is None:
    print(f"Error loading scissors image from {scissors_image_path}")

gesture_images = {
    "Stein": rock_image,
    "Papier": paper_image,
    "Schere": scissors_image
}

data = {"Stein": [], "Schere": [], "Papier": []}

def calculate_angle(a, b, c):
    ba = np.array([a.x - b.x, a.y - b.y])
    bc = np.array([c.x - b.x, c.y - b.y])
    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    angle = np.arccos(cosine_angle)
    return np.degrees(angle)

def extract_features(hand_landmarks):
    angle_thumb_index = calculate_angle(hand_landmarks.landmark[2], hand_landmarks.landmark[3], hand_landmarks.landmark[4])
    angle_index = calculate_angle(hand_landmarks.landmark[5], hand_landmarks.landmark[6], hand_landmarks.landmark[8])
    angle_middle = calculate_angle(hand_landmarks.landmark[9], hand_landmarks.landmark[10], hand_landmarks.landmark[12])
    angle_ring = calculate_angle(hand_landmarks.landmark[13], hand_landmarks.landmark[14], hand_landmarks.landmark[16])
    angle_pinky = calculate_angle(hand_landmarks.landmark[17], hand_landmarks.landmark[18], hand_landmarks.landmark[20])
    return [angle_thumb_index, angle_index, angle_middle, angle_ring, angle_pinky]

def load_data():
    with open(os.path.join(data_dir, 'gesture_data.pkl'), 'rb') as f:
        return pickle.load(f)

def classify_hand_gesture(model, lb, hand_landmarks):
    features = np.array([extract_features(hand_landmarks)])
    prediction = model.predict(features)
    gesture = lb.inverse_transform(prediction)[0]
    return gesture

# AI decision
def ai_decision():
    return random.choice(["Stein", "Schere", "Papier"])

# Determine winner
def determine_winner(ai_choice, player_choice):
    if ai_choice == player_choice:
        return "Unentschieden"
    elif (ai_choice == "Stein" and player_choice == "Schere") or \
         (ai_choice == "Schere" and player_choice == "Papier") or \
         (ai_choice == "Papier" and player_choice == "Stein"):
        return "AI"
    elif (player_choice == None):
        return ""
    else:
        return "Spieler"

# Open webcam again to recognize gestures
cap = cv2.VideoCapture(camera_index)
model = tf.keras.models.load_model('gesture_model.h5')
with open('label_binarizer.pkl', 'rb') as f:
    lb = pickle.load(f)

# Points
ai_points = 0
player_points = 0

# Time for AI decision
last_decision_time = time.time()
decision_interval = 5
last_countdown_time = time.time()
countdown_duration = 3
countdown_active = False
ai_choice_display = None
player_choice = None

# Set up fullscreen window
cv2.namedWindow('Hand Tracking', cv2.WND_PROP_FULLSCREEN)
cv2.setWindowProperty('Hand Tracking', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break

    frame = cv2.flip(frame, 1)
    
   
    rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    results = hands.process(rgb_frame)

    if results.multi_hand_landmarks:
        # Process only the first hand detected
        hand_landmarks = results.multi_hand_landmarks[0]
        mp_drawing.draw_landmarks(frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)
        gesture = classify_hand_gesture(model, lb, hand_landmarks)
        wrist = hand_landmarks.landmark[0]
        height, width, _ = frame.shape
        cx, cy = int(wrist.x * width), int(wrist.y * height)
        cv2.putText(frame, gesture, (cx, cy), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
        player_choice = gesture  # Save player choice

    current_time = time.time()
    if current_time - last_decision_time >= decision_interval:
        if not countdown_active:
            ai_choice_display = None  # Reset ai_choice_display
            countdown_active = True
            last_countdown_time = current_time
            player_choice = None  # Clear player's last recognized gesture

    if countdown_active:
        countdown_remaining = countdown_duration - (current_time - last_countdown_time)
        if countdown_remaining > 0:
            cv2.putText(frame, f'{int(countdown_remaining)}', (frame.shape[1]//2 - 10, frame.shape[0]//2), cv2.FONT_HERSHEY_SIMPLEX, 3, (255, 255, 255), 5, cv2.LINE_AA)
        else:
            ai_choice = ai_decision()
            ai_choice_display = ai_choice
            last_decision_time = current_time
            countdown_active = False

            # Determine winner
            winner = determine_winner(ai_choice, player_choice)
            if winner == "AI":
                ai_points += 1
            elif winner == "Spieler":
                player_points += 1

    if ai_choice_display:
        if player_choice is None:
            #cv2.putText(frame, 'Please choose something so that I can beat you :D', (frame.shape[1]//2 - 200, frame.shape[0]//2 + 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
            cv2.putText(frame, 'KI: Please choose something', (85, 400), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
            cv2.putText(frame, 'so that I can beat you :D', (140, 450), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
        else:
            ai_choice_image = gesture_images[ai_choice_display]
            if ai_choice_image is not None:
                ai_choice_image_resized = cv2.resize(ai_choice_image, (100, 100))
                x_offset = 10  # Adjust this based on your layout
                y_offset = 60  # Adjust this based on your layout
                frame[y_offset:y_offset+100, x_offset:x_offset+100] = ai_choice_image_resized

    cv2.putText(frame, f'KI: {ai_points}', (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
    cv2.putText(frame, f'Spieler: {player_points}', (frame.shape[1] - 160, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
    #cv2.putText(frame,  f'{ai_points} : {player_points}', (frame.shape[1]//2 - 150, 40), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 165, 0), 2, cv2.LINE_AA)

    cv2.imshow('Hand Tracking', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()

