import cv2
import mediapipe as mp
import numpy as np
import pickle
import tensorflow as tf
import random
import time
import os
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
 
# Initialisiere die Händeerkennung
mp_hands = mp.solutions.hands
hands = mp_hands.Hands(min_detection_confidence=0.7, min_tracking_confidence=0.7)
mp_drawing = mp.solutions.drawing_utils
 
# Videoaufnahme von der Webcam starten
cap = cv2.VideoCapture(1)
 
# Verzeichnisse für die Daten
data_dir = r'./data'
if not os.path.exists(data_dir):
    os.makedirs(data_dir)
 
data = {"Stein": [], "Schere": [], "Papier": []}
 
def calculate_angle(a, b, c):
    ba = np.array([a.x - b.x, a.y - b.y])
    bc = np.array([c.x - b.x, c.y - b.y])
    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    angle = np.arccos(cosine_angle)
    return np.degrees(angle)
 
def extract_features(hand_landmarks):
    angle_thumb_index = calculate_angle(hand_landmarks.landmark[2], hand_landmarks.landmark[3], hand_landmarks.landmark[4])
    angle_index = calculate_angle(hand_landmarks.landmark[5], hand_landmarks.landmark[6], hand_landmarks.landmark[8])
    angle_middle = calculate_angle(hand_landmarks.landmark[9], hand_landmarks.landmark[10], hand_landmarks.landmark[12])
    angle_ring = calculate_angle(hand_landmarks.landmark[13], hand_landmarks.landmark[14], hand_landmarks.landmark[16])
    angle_pinky = calculate_angle(hand_landmarks.landmark[17], hand_landmarks.landmark[18], hand_landmarks.landmark[20])
    return [angle_thumb_index, angle_index, angle_middle, angle_ring, angle_pinky]
 
def save_data():
    with open(os.path.join(data_dir, 'gesture_data.pkl'), 'wb') as f:
        pickle.dump(data, f)
 
def load_data():
    with open(os.path.join(data_dir, 'gesture_data.pkl'), 'rb') as f:
        return pickle.load(f)
 
def train_model():
    # Lade die Daten
    data = load_data()
   
    # Bereite die Daten vor
    X = []
    y = []
    for gesture, features in data.items():
        X.extend(features)
        y.extend([gesture] * len(features))
 
    X = np.array(X)
    y = np.array(y)
   
    # One-hot encoding der Labels
    from sklearn.preprocessing import LabelBinarizer
    lb = LabelBinarizer()
    y = lb.fit_transform(y)
   
    # Modell erstellen
    model = Sequential([
        Dense(64, activation='relu', input_shape=(5,)),
        Dropout(0.5),
        Dense(32, activation='relu'),
        Dropout(0.5),
        Dense(3, activation='softmax')
    ])
   
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    model.fit(X, y, epochs=50, batch_size=8)
   
    model.save('gesture_model.h5')
    with open('label_binarizer.pkl', 'wb') as f:
        pickle.dump(lb, f)
 
# collecting = False
# current_gesture = None
 
# while cap.isOpened():
#     ret, frame = cap.read()
#     if not ret:
#         break
 
#     frame = cv2.flip(frame, 1)
#     rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
#     results = hands.process(rgb_frame)
 
#     if results.multi_hand_landmarks:
#         for hand_landmarks in results.multi_hand_landmarks:
#             mp_drawing.draw_landmarks(frame, hand_landmarks, mp_hands.HAND_CONNECTIONS)
#             if collecting and current_gesture:
#                 data[current_gesture].append(extract_features(hand_landmarks))
#                 cv2.putText(frame, f'Collecting {current_gesture}', (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
 
#     cv2.imshow('Hand Tracking', frame)
 
#     key = cv2.waitKey(1) & 0xFF
#     if key == ord('q'):
#         break
#     elif key == ord('s'):
#         collecting = True
#         current_gesture = "Stein"
#     elif key == ord('c'):
#         collecting = True
#         current_gesture = "Schere"
#     elif key == ord('p'):
#         collecting = True
#         current_gesture = "Papier"
#     elif key == ord('z'):
#         collecting = False
#         save_data()
#         break
 
# cap.release()
# cv2.destroyAllWindows()

train_model()