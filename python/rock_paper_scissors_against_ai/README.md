# Schere Stein Papier KI

Dieses Projekt ermöglicht es Ihnen, gegen eine künstliche Intelligenz (KI) Schere, Stein, Papier zu spielen. Die KI verwendet ein trainiertes Modell, um Ihre Handgesten zu erkennen und entsprechend zu reagieren.

## Vorbereitung

Bevor Sie das Spiel starten, müssen Sie die KI mit Bildern Ihrer Handgesten trainieren. Folgen Sie den nachstehenden Anweisungen, um das Training und das Spiel durchzuführen.

### Training der KI

1. Öffnen Sie die Datei `CollectAndTrain.py`.
2. Geben Sie in der Variable `data_dir` das Verzeichnis an, in dem die gesammelten Bilder und das trainierte Modell gespeichert werden sollen.
   ```python
   data_dir = 'Ihr/Verzeichnis/Pfad'
3. Starten Sie das Programm.
4. Drücken Sie die Taste S, um Bilder für die Geste "Stein" zu sammeln.
5. Drücken Sie die Taste C, um Bilder für die Geste "Schere" zu sammeln.
6. Drücken Sie die Taste P, um Bilder für die Geste "Papier" zu sammeln.

Bei jedem Klick wird ein Bild gemacht. Versuchen Sie verschiedene Positionen und Winkel Ihrer Handgesten, um die KI so gut wie möglich zu trainieren.

# Starten des Spiels

1. Öffnen Sie die Datei `RPS.py`.
2. Geben Sie in der Variable `data_dir` das Verzeichnis an, in dem die gesammelten Bilder und das trainierte Modell gespeichert werden sollen.
   ```python
   data_dir = 'Ihr/Verzeichnis/Pfad'^
3. Starten Sie das Programm.
4. Jetzt sollte alles funktionieren, und Sie können gegen die KI Schere, Stein, Papier spielen.

# Anforderungen

Stellen Sie sicher, dass Sie die notwendigen Bibliotheken installiert haben.

# Hinweise

Achten Sie darauf, dass Ihre Handgesten gut sichtbar und vor einem einheitlichen Hintergrund aufgenommen werden, um die Erkennungsgenauigkeit der KI zu verbessern.

Sammeln Sie eine ausreichende Anzahl von Bildern für jede Geste aus verschiedenen Winkeln und Positionen.

Viel Spaß beim Spielen gegen die KI!